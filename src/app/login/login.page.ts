import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  main = true;
  starts = false;

  constructor() { }

  ngOnInit() {
   
  }

  inicios(){
    this.main = false;
    this.starts = true;
  }

}
